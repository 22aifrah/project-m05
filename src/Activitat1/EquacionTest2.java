package Activitat1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EquacionTest2 {

	@Test
	void test() {
		assertEquals("(-1.0)", Equacion.solution(1.0,2.0,1.0));
		assertEquals("(-1.0,1.0)", Equacion.solution(1.0,0.0,-1.0));
		assertEquals("(-1.0i,1.0i)", Equacion.solution(1.0,0.0,1.0)); 
	}

}
