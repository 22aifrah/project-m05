package Activitat1;

import java.util.Scanner;

public class Equacion {
	public static void main(String[] args) {
		System.out.println("aX^2 + bX + C = 0");
		Scanner sc = new Scanner(System.in);
		double a = sc.nextDouble(),b = sc.nextDouble(),c = sc.nextDouble();
		System.out.println(solution(a, b, c));
		sc.close();
	}
	public static String solution(double a , double b , double c) {
		double delta = b*b - 4*a*c;
		if (a == 0 ) {
			if(b != 0) {
				return "("+Double.toString(-c/b)+")";
			}
			return "no hay solucion";
		}else if (delta == 0) {
		    return "("+Double.toString(-b/(2*a))+")";
		}
		else if(delta > 0 ) {
			return "("+Double.toString((-b-Math.sqrt(delta))/(2*a))+","+Double.toString((-b+Math.sqrt(delta))/(2*a))+")";
		}else {
			if((-b)/(2*a) == 0) {
				return "("+Double.toString(-(Math.sqrt(-delta)/(2*a)))+"i"+","+Double.toString(Math.sqrt(-delta)/(2*a))+"i"+")";
			}else {
				return "("+Double.toString((-b)/(2*a))+"-"+ Double.toString(Math.abs(Math.sqrt(-delta)/(2*a)))+"i"+","+Double.toString((-b)/(2*a))+"+"+ Double.toString(Math.sqrt(-delta)/(2*a))+"i"+")";
			}
		}
	}

}
