package Activitat1;

import java.util.Scanner;

public class Trace {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); 
		int linias = sc.nextInt();
		int colomnas = sc.nextInt();
		int [][] array = new int[linias][colomnas];
		for (int i = 0 ; i < linias ; i++) {
			for(int j = 0 ; j < colomnas ; j++) {
				array[i][j] = sc.nextInt();
			}
		}
		System.out.println("trace = "+trase(linias,colomnas,array));
		sc.close();
	}
	public static int trase(int l, int c,int [][] array) {
		int s = 0;
		for (int i = 0 ; i < l ; i++) {
			for(int j = 0 ; j < c ; j++) {
				if(i == j)
					s += array[i][j]; 
			} 
		}
		return s;
	}

}
