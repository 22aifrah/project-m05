package Activitat1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TraceTest {

	@Test
	void test() {
		int [][] array = {{1,0,0},{0,1,0},{0,0,1}};
		assertEquals(3,Trace.trase(3, 3, array));
	} 

}
